#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>

#include <linux/kernel.h>

#include <linux/iio/iio.h>

#define SI7057_CMD_QUERY_DEVICE_NO_RESP1 0x5D
#define SI7057_CMD_QUERY_DEVICE_NO_RESP2 0x80
#define SI7057_CMD_QUERY_DEVICE_WITH_RESP1 0xEF
#define SI7057_CMD_QUERY_DEVICE_WTIH_RESP2 0x80

#define SI7057_QUERY_DEVICE_RESP1 0x00
#define SI7057_QUERY_DEVICE_RESP2 0x47
#define SI7057_QUERY_DEVICE_RESP3 0x2B

#define SI7057_TEMP_MEASURE_NORMAL_HOLD1 0x7C
#define SI7057_TEMP_MEASURE_NORMAL_HOLD2 0xA2
#define SI7057_TEMP_MEASURE_NORMAL_NOHOLD1 0x78
#define SI7057_TEMP_MEASURE_NORMAL_NOHOLD2 0x66

#define SI7057_TEMP_MEASURE_FAST_HOLD1 0x64
#define SI7057_TEMP_MEASURE_FAST_HOLD2 0x58
#define SI7057_TEMP_MEASURE_FAST_NOHOLD1 0x60
#define SI7057_TEMP_MEASURE_FAST_NOHOLD2 0x9C

#define SI7057_TEMP_SCALE 175 / (2^16)
#define SI7057_TEMP_OFFSET -45

struct si7057 {
	struct i2c_client *client;
};

static int si7057_read_raw(struct iio_dev *indio_dev,
			   struct iio_chan_spec const *chan,
			   int *val, int *val2, long mask)
{
	int ret;
	struct si7057 *priv_data = iio_device_get_drvdata(indio_dev);

	switch (mask) {
	case IIO_CHAN_INFO_RAW:
		ret = i2c_smbus_read_word_swapped(priv_data->client, SI7057_TEMP_MEASURE_NORMAL_HOLD1);
		return ret;
	case IIO_CHAN_INFO_SCALE:
		*val = SI7057_TEMP_SCALE;
		return IIO_VAL_INT;
        case IIO_CHAN_INFO_OFFSET:
                *val = SI7057_TEMP_OFFSET;
                return IIO_VAL_INT;
	default:
		return -EINVAL;
	}
}

static const struct iio_info si7057_info = {
	.read_raw = &si7057_read_raw,
};

static const struct iio_chan_spec si7057_channel[] = {
     {
         .type = IIO_TEMP,
         .info_mask_separate = BIT(IIO_CHAN_INFO_PROCESSED),
     },
};

int si7057_probe(struct i2c_client *client)
{
	int ret; /* Temporary storage value for i2c reads */
	struct iio_dev *indio_dev;
	struct si7057 *priv_data;

	// This function is called when an I2C device is detected.
	printk(KERN_INFO "My I2C device detected\n");

	ret = i2c_smbus_read_word_data(client, SI7057_CMD_QUERY_DEVICE_WITH_RESP1);

	if (ret < 0) {
		return dev_err_probe(&client->dev, ret, "Failed to query device!");
	} else if (ret != SI7057_QUERY_DEVICE_RESP1 && ret != SI7057_QUERY_DEVICE_RESP2 &&
		   ret != SI7057_QUERY_DEVICE_RESP3) {
		return dev_err_probe(&client->dev, -ENODEV, "Device sent unknown query response!");
	}

	indio_dev = devm_iio_device_alloc(&client->dev, 0);

	if (!indio_dev)
		return -ENOMEM;

	priv_data = iio_priv(indio_dev);
        i2c_set_clientdata(client, indio_dev);
	priv_data->client = client;

	indio_dev->modes = INDIO_DIRECT_MODE;
	indio_dev->channels = si7057_channel;
	indio_dev->num_channels = ARRAY_SIZE(si7057_channel);
	indio_dev->name = "si7057";
	indio_dev->info = &si7057_info;

	return devm_iio_device_register(&client->dev, indio_dev);
}

static const struct i2c_device_id si7057_id[] = {
    { "si7057", 0 },
    { "si7058", 0 },
    { "si7059", 0 },
    { }
};
MODULE_DEVICE_TABLE(i2c, si7057_id);

static const struct of_device_id si7057_dt_ids[] = {
	{ .compatible = "si7057,si7058,si7059" },
	{ }
};
MODULE_DEVICE_TABLE(of, si7057_dt_ids);

static struct i2c_driver si7057_driver = {
    .driver = {
        .name = "si7057_driver",
	.of_match_table = si7057_dt_ids,
        .owner = THIS_MODULE,
    },
    .probe = si7057_probe,
    .id_table = si7057_id,
};

module_i2c_driver(si7057_driver);

MODULE_AUTHOR("Yusuf Khan <yusisamerican@gmail.com>");
MODULE_DESCRIPTION("SiLabs Si7057 driver");
MODULE_LICENSE("GPL");
